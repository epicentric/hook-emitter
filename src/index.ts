import 'reflect-metadata';

export function getHooks<TOptions extends HookOptions<TEnum>, TEnum>(symbol: Symbol, target: any): { name: string, options: TOptions }[]
{
    return Reflect.getMetadata(symbol, target) || [];
}

export function createHookDecorator<TOptions extends HookOptions<TEnum>, TEnum>(symbol: Symbol)
{
    return function hookDecorator(options: TOptions)
    {
        return function(target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) {
            const current = getHooks(symbol, target);
            current.push({ name: propertyKey, options });
            Reflect.defineMetadata(symbol, current, target);
        };
    };
}

export type HookFunction<TArgs, TEnum> = (args: TArgs, type: TEnum) => Promise<void>|void;

export interface HookOptions<TEnum>
{
    type?: TEnum;
    types?: TEnum[];
    priority?: number;
}

interface RegisteredHook<TArgs, TEnum, TOptions>
{
    func: HookFunction<TArgs, TEnum>;
    options: TOptions;
}

export class HookEmitter<TEnum, TArgs, TOptions extends HookOptions<TEnum>>
{
    private readonly hooks: RegisteredHook<TArgs, TEnum, TOptions>[] = [];

    // Type inferred by webstorm
    public readonly hook: (options: TOptions) => (target: any, propertyKey: string, descriptor: TypedPropertyDescriptor<any>) => void;

    public constructor(private readonly symbol: Symbol)
    {
        this.hook = createHookDecorator<TOptions, TEnum>(symbol);
    }

    public register(func: HookFunction<TArgs, TEnum>, options: TOptions): void
    {
        const opts = Object.assign({
            priority: 0
        }, options);

        this.hooks.push({ func, options: opts });
        // Sort descending
        this.hooks.sort((a, b) => b.options.priority! - a.options.priority!);
    }

    public registerClass(instance: { [key: string]: any }): void
    {
        const proto = Object.getPrototypeOf(instance);
        const hooks = getHooks<TOptions, TEnum>(this.symbol, proto);

        for (const hook of hooks)
        {
            if ('type' in hook.options)
                hook.options.types = [hook.options.type!];
            else if (!('types' in hook.options))
                throw new Error(`You need to specify either 'type' or 'types' in the hook decorator`);

            const func = (instance[hook.name] as HookFunction<TArgs, TEnum>).bind(instance);

            this.register(func, hook.options);
        }
    }

    public async emit(type: TEnum, args: TArgs, filter?: (hook: RegisteredHook<TArgs, TEnum, TOptions>) => boolean): Promise<void>
    {
        for (const hook of this.hooks)
        {
            const typeMatches = hook.options.type === type;
            const typesContains = (hook.options.types && hook.options.types.includes(type));
            const filterMatches = !filter || filter(hook);

            if ((typeMatches || typesContains) && filterMatches)
            {
                const promise = hook.func(args, type);

                if (promise && promise.then)
                    await promise;
            }
        }
    }
}